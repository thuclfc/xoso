//number random

quayso('.btn_quay0','#num_0');
quayso('.btn_quay1','#num_1');
quayso('.btn_quay2','#num_2');
quayso('.btn_quay3','#num_3');
quayso('.btn_quay4','#num_4');
quayso('.btn_quay5','#num_5');

function quayso(button, number) {
    $(button).click(function () {
        let _this = $(this);
        let max = parseInt(_this.attr('max'));
        let min = parseInt(_this.attr('min'));
        let number_random = Math.floor((Math.random() * max) + min);
        $(''+number+' .number').addClass('spiningY');
        var tn = setInterval(function () {
            var number_rd = Math.floor(Math.random() * 45) + 0;
            $(''+number+' .number').html(`<span>${number_rd}</span>`);
        },400);

        setTimeout(function () {
            clearInterval(tn);
            $(this).attr('disabled','disabled');
            $(''+number+' .number').html(`<span class="animated zoomIn">${number_random}</span>`);
            $('.number').removeClass('spiningY');
        },5000);
    });
}

$('.btn_quay_new').click(function () {
   $('.number').html(`<span class="animated zoomIn">0</span>`);
});